Image processor
====================
High performance java app for download, resize and upload to s3 images.

Requirements:
-------------
JDK or JRE 8 or higher

Configuration:
--------------
Write your values to main.properties
Configure S3 using manual: https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
Write images urls to url.csv
