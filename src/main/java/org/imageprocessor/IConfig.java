package org.imageprocessor;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:main.properties"})
public interface IConfig extends Config {

    @Key("threads.upload")
    int uploadThreadsCount();

    @Key("threads.downloadAndProcess")
    int downloadAndProcessThreadsCount();

    @Key("target.size")
    int targetSize();

    @Key("s3.bucket.name")
    String s3BucketName();

    @Key("urls.file.path")
    String urlsFilePath();
}
