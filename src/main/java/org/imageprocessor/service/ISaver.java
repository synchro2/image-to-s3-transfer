package org.imageprocessor.service;

import java.util.concurrent.TimeUnit;

interface ISaver<T, V> {
    void save(T path, V object) throws Exception;

    void shutdown();

    boolean awaitSaveComplete(long time, TimeUnit timeUnit) throws InterruptedException;
}
