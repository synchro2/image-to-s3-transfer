package org.imageprocessor.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProcessAndSaveTemplate<T, V> {
    private final IProcessor<T, V> processor;
    private final ISaver<T, V> uploader;
    private final ExecutorService service;

    private static final Logger log = LoggerFactory.getLogger(ProcessAndSaveTemplate.class);

    public ProcessAndSaveTemplate(IProcessor<T, V> processor, ISaver<T, V> uploader, int processThreadCount) {
        this.processor = processor;
        this.uploader = uploader;
        this.service = Executors.newFixedThreadPool(processThreadCount);
    }

    public ProcessAndSaveTemplate(IProcessor<T, V> processor, ISaver<T, V> uploader) {
        this(processor, uploader, Runtime.getRuntime().availableProcessors());
    }

    public Future addImage(T path) {
        return service.submit(() -> process(path));
    }

    private void process(T path) {
        V object;
        try {
            object = processor.process(path);
            log.info("Image {} resized", path);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        try {
            uploader.save(path, object);
            log.info("Image {} uploaded to s3", path);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public boolean shutdownGracefully(long timeOut, TimeUnit timeUnit) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        long timeOutMillis = timeUnit.toMillis(timeOut);

        service.shutdown();
        if (!service.awaitTermination(timeLeft(timeOutMillis, startTime), TimeUnit.MILLISECONDS)) {
            return false;
        }

        if (!uploader.awaitSaveComplete(timeLeft(timeOutMillis, startTime), TimeUnit.MILLISECONDS)) {
            return false;
        }
        uploader.shutdown();

        return true;
    }

    private long timeLeft(long timeOutMillis, long startTime) {
        return timeOutMillis - (System.currentTimeMillis() - startTime);
    }
}
