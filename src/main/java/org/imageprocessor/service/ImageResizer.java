package org.imageprocessor.service;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

public class ImageResizer implements IProcessor<String, InputStream> {
    private final int targetImageSize;

    public ImageResizer(int targetImageSize) {
        this.targetImageSize = targetImageSize;
    }

    @Override
    public InputStream process(String path) throws Exception {
            URL image = new URL(path);
            BufferedImage bufferedImage = ImageIO.read(image);
            BufferedImage scaledImage = Scalr.resize(bufferedImage, targetImageSize);
            bufferedImage.flush();
            String format = getFormat(image.getPath());
            ByteArrayOutputStream os = new ByteArrayOutputStream(4096);
            ImageIO.write(scaledImage, format, os);
            scaledImage.flush();
            os.close();
            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
            is.close();
            return is;
    }

    private String getFormat(String path) {
        return path.substring(path.lastIndexOf(".") + 1);
    }
}
