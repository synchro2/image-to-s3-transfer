package org.imageprocessor.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class S3Uploader implements ISaver<String, InputStream> {
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final String bucketName;
    private final TransferManager transferManager;
    private final Queue<Upload> uploads = new ConcurrentLinkedQueue<>();

    private static final Logger log = LoggerFactory.getLogger(S3Uploader.class);

    public S3Uploader(String bucketName, TransferManager transferManager) {
        this.bucketName = bucketName;
        this.transferManager = transferManager;
        scheduler.scheduleWithFixedDelay(this::checkUploads, 1, 1, TimeUnit.SECONDS);
    }

    public S3Uploader(String bucketName) {
        this(bucketName, TransferManagerBuilder.defaultTransferManager());
    }

    private void checkUploads() {
        Upload upload;
        while ((upload = uploads.peek()) != null) {
            try {
                upload.waitForCompletion();
                uploads.remove();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void save(String path, InputStream inputStream) throws Exception {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(inputStream.available());

        Upload upload = transferManager.upload(bucketName, getName(path), inputStream, metadata);
        uploads.add(upload);
    }

    private String getName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    @Override
    public void shutdown() {
        transferManager.shutdownNow();
        scheduler.shutdownNow();
    }

    @Override
    public boolean awaitSaveComplete(long time, TimeUnit timeUnit) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        long millis = timeUnit.toMillis(time);
        while (hasTime(startTime, millis)) {
            if (uploads.isEmpty()) {
                return true;
            }
            Thread.sleep(200);
        }
        return false;
    }

    private boolean hasTime(long startTime, long time) {
        return System.currentTimeMillis() - startTime < time;
    }
}
