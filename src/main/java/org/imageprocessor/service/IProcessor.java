package org.imageprocessor.service;

interface IProcessor<T, V> {
    V process(T object) throws Exception;
}
