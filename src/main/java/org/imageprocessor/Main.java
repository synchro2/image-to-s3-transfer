package org.imageprocessor;

import com.amazonaws.client.builder.ExecutorFactory;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import org.aeonbits.owner.ConfigFactory;
import org.imageprocessor.service.ImageResizer;
import org.imageprocessor.service.ProcessAndSaveTemplate;
import org.imageprocessor.service.S3Uploader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


public class Main {
    private static ProcessAndSaveTemplate<String, InputStream> service;

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());
        IConfig config = ConfigFactory.create(IConfig.class);

        ExecutorFactory factory = () -> Executors.newFixedThreadPool((config.uploadThreadsCount()));
        TransferManager transferManager = TransferManagerBuilder.standard().withExecutorFactory(factory).build();
        S3Uploader s3Uploader = new S3Uploader(config.s3BucketName(), transferManager);
        ImageResizer imageResizer = new ImageResizer(config.targetSize());
        service = new ProcessAndSaveTemplate<>(imageResizer, s3Uploader, config.downloadAndProcessThreadsCount());

        Path path = Paths.get(config.urlsFilePath());
        Set<String> urlStrings = Files.lines(path).collect(Collectors.toSet());
        List<Future> futures = new ArrayList<>(urlStrings.size());

        log.info("Loaded {} urls", urlStrings.size());

        for (String url : urlStrings) {
            Future future = service.addImage(url);
            futures.add(future);
        }

        for (Future future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                log.error(e.getMessage(), e);
            }
        }

        System.exit(0);
    }

    static class ShutdownHook extends Thread {
        @Override
        public void run() {
            try {
                boolean success = service.shutdownGracefully(20, TimeUnit.SECONDS);
                if (success) {
                    log.info("Shutdown successful");
                } else {
                    log.warn("Not enough time for shutdown");
                }
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}
